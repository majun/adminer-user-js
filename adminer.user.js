// ==UserScript==
// @name           Adminer SQL editor
// @namespace      http://majun.com/adminer-sql-editor
// @description    Add SQL syntax highlighting editor to all sql areas
// @version        0.2.1
// @downloadURL    https://bitbucket.org/majun/adminer-user-js/raw/master/adminer.user.js
// @copyright      2013+, Martin Junger
// @include        http://adminer/*
//    wysiwyg CodeMirror
// @require        http://codemirror.net/lib/codemirror.js
// @resource       cssCm http://codemirror.net/lib/codemirror.css
//    syntax
// @require        http://codemirror.net/mode/sql/sql.js
//    addon: active line
// @require        http://codemirror.net/addon/selection/active-line.js
//    addon: match brackets
// @require        http://codemirror.net/addon/edit/matchbrackets.js
//    addon: vim
// @require        http://codemirror.net/addon/dialog/dialog.js
// @resource       cssCmVim http://codemirror.net/addon/dialog/dialog.css
// @require        http://codemirror.net/addon/search/searchcursor.js
// @require        http://codemirror.net/mode/clike/clike.js
// @require        http://codemirror.net/keymap/vim.js
//
// @run-at         document-end
// ==/UserScript==

// Wysiwyg
// load styles
GM_addStyle(GM_getResourceText('cssCm'));
// border
customCSS = '.CodeMirror { border: 1px solid black; line-height: 1.3em; } ';
// active line
customCSS += '.CodeMirror-activeline-background { background: #e8f2ff !important; } ';
// infinity scroll
customCSS += '.CodeMirror { height: auto; } .CodeMirror-scroll { overflow-y: hidden; overflow-x: auto; } ';
GM_addStyle(customCSS);
// vim
GM_addStyle(GM_getResourceText('cssCmVim'));

// select SQL mode by url param
var sqlEngine = location.search.split('?')[1].split('=')[0];
var sqlMode = 'text/x-sql';
if(sqlEngine == 'pgsql') {
    sqlMode = 'text/x-mysql'; // none for PostgreSQL
    //sqlMode = 'text/x-plsql';
} else {
    sqlMode = 'text/x-mysql';
}

// get textarea
var area = document.getElementsByClassName('sqlarea')[0];

// insert editor
area.removeAttribute('onkeydown');
var sqleditor;
function initCm() {
    sqleditor = CodeMirror.fromTextArea(area, {
        mode: sqlMode,
        indentUnit: 4,
        indentWithTabs: false,
        smartIndent: true,
        lineNumbers: true,
        matchBrackets : true,
        autofocus: true,
        styleActiveLine: true,
        viewportMargin: Infinity,
        vimMode: true,
        showCursorWhenSelecting: true
    });
    sqleditor.on('change', function () {
        var content = sqleditor.getValue();
        area.value = content;
    });
};
initCm();
